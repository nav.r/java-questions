## Java Quiz

1. How can you create a sequential stream
	 
	 - [ ] Collection.stream() 	 
	 - [ ] Collection.sequentialStream() 	
	 - [ ] Collection.parallelStream()   

2. You have the following class:
	
			public class Holder
			{
				public Holder(int capacity) { }
			}
	you must extent this class with the following class:
		
			public class SmartHolder extends Holder {
				public SmartHolder (int capacity) {
					...
				}
			}
	what code do you need to add to the `SmartHolder`  constructor?
	 - [ ] super(capacity); 
	 - [ ] this(); 
	 - [ ] this(capacity); 
	 - [ ] super();
	
3. You want to convcatenate several instances of `String` together. What is the most efficient way to do it?

	- [ ] Create a `StringBuilder`. Add the strings in it, and convert it into the result `String`
	- [ ] Use the `+` arithmetic symbol
	- [ ] Use the `concat()` method of the `String` class
	- [ ] Put the instances of `String` in a list, then join the list using a `StringJoiner`

4. What is true of the following statement?
			
		String s = "Hello World.";

	 - [ ] A primitive type is being declared
	 - [ ] This is an assignment statement
	 - [ ] You can mutate the string object referred by variable `s` later in the program
	 - [ ] The following line of code will result in an output of 11
				
			System.out.println(s.length());
	

5. What is the difference between `implements` and `extends`?

	 - [ ] `extends`  allows the interface to inherit features from an interface, while `implements`  allows an interface to access features from a class.
	 - [ ] `extends` allows a class to access features from an interface, while `implements` allows a class to inherit features from a class
	 - [ ] `extends` allows a class to inherit features from a class, while `implements` allows a class to access features from an interface.
	 - [ ] `extends` allows an interface to inherit features from a class, while `implements` allows an  interface to access featires from an interface

6. You have a Person class with an age property of `int`. You have a stream of instances of this class and must count the numberof people older than 20. What collector can you use for that>
	 - [ ] `Collectors.filtering(age -> age > 20, summing())`
	 - [ ] `Collectors.filtering(age -> age > 20, counting())`
	 - [ ] `Collectors.filtering(p -> p.getAge > 20, summing())`
	 - [ ] `Collectors.filtering(p -> p.getAge > 20, counting())`

7. you have an `int` variable alled ` i ` and the variable has been initialized, where necessary. Which snippet of code will result in a syntax error?
	 - [ ] `for (i = 0; i < 10; i++) { }`
	 - [ ] `while (i++ < 10) { }`
	 - [ ] `repeat { } until (i++ < 10 );`
	 - [ ] `do { } while (i++ < 10);`

8. You must create a class that innherits from the type if an interface. What keyword do you use?
	 - [ ] `extends`
	 - [ ] `inherits`
	 - [ ] `implements`
	 - [ ] `overrides`

9. Yopu have two streams names `stream1` and `stream2`. Which method of `Stream` will create a new stream with all the elements of `stream1` followed by all the elements of `stream2`?
	 - [ ] `concat()`
	 - [ ] `flatMap()`
	 - [ ] `collect()`
	 - [ ] `filter()`

10. Which method can you use to find out if an iteration has more elements in its collection?
	 - [ ] `next()`
	 - [ ] `!isEmpty()`
	 - [ ] `isEmpty()`
	 - [ ] `hasNext()`

11. How can you declare a float variable of ` pi `, with ` 3.14 ` as its initial value?
	 - [ ] `pi = 3.14f;`
	 - [ ] `pi = 3.14;`
	 - [ ] `float pi = 3.14;`
	 - [ ] `Float pi = 3.14;`

12. Which is a valid switch statement where the variable ` s ` is a String?

	 - [ ] Solution 1

		    switch (s) {
		    	case 5:
		    	case 6: 
		    		System.out.println("s is 5 or 6");
		    }

	 - [ ] Solution2
	 
		    switch (s) {
		    	case "5":
			    	continue;
		    	case "6": 
		    		System.out.println("s is 6");
		    }

	 - [ ] Solution 3
	 
		    switch (s) {
		    	case "5":
			    	break;
		    	case "6": 
		    		System.out.println("s is 6");
		    }

	 - [ ] Solution 4
	 
		    switch (s) {
		    	case 5:
		    	case "6": 
		    		System.out.println("s is 5 or 6");
		    }

13. Given the following code:

		ArrayList<String> al = new ArrayList<>();
		al.add("A");
		al.add("B");
		al.add("C");
		al.add("D");
		ArrayList<String> alClone = (ArrayList<String>) al.Clone();
		// Your code here
		System.out.println(al);
		System.out.println(alClone);
		
	Which line of code can you add to produce the following output?
	[B, C, D]
	[A, B, C, D, E]

	 - [ ] Solution 1
			
		    alClone.remove("A");
		    al.add("E");

	 - [ ] Solution 2
			
		    al.remove("A");
		    al.add("E");

	 - [ ] Solution 3
			
		    al.remove("A");
		    alClone.add("E");

	 - [ ] Solution 4
			
		    alClone.remove("A");
		    alClone.add("E");

14. You have the following Java code:
		
		 String txt1 = "The quick brown fox ";
	 	 String txt2 = "jumped over ";
		 String txt3 = "the lazy dog ";
	 	 System.out.println(txt1 + txt2 + txt3);
	How can you print out the same sentence using `String` method?

	 - [ ] `System.out.println((txt1 + txt2).endsWith(txt3));`
	 - [ ] `System.out.println(txt1.concat(txt2).concat(txt3));`
	 - [ ] `System.out.println(txt1.append(txt2).append(txt3));`
	 - [ ] `System.out.println(txt1.jstartsWith(txt2 + txt3));`

15.  Which keyword is illegal to use as a visibility modifier for class members?
		- [ ] `public`
		- [ ] `package`
		- [ ] `private`
		- [ ] `protected`

16. what does the .class file contain?
	- [ ] Instructions written in XML
	- [ ] Bytecode, dependent of the machine the Java application will be deployed on
	- [ ] CPU assembly code
	- [ ] Bytecode, independent of the machine the Java application will be deployed on 

17. You have 2 interfaces called Interface1 and Interface2. You  want Interface2 to be able to access the methods in Interface1. How can you do this?
	 - [ ] `public interface Interface2 implements Interface1`
	 - [ ] `public interface Interface2`
	 - [ ] `public interface Interface2 extends Interface1`
	 - [ ] `public interface Interface2 overrides Interface1`

18.  What will be the output when you run the following class heirarchy?

		    abstract class Animal {
			    public void talk() {
					    System.out.println("I am an animal");
			    }
		    }
		    
		    public class Pig extends Animal {
			    public void talk() {
					    System.out.println("I am a pig");
			    }
		    }
		    
		    public class Dog extends Animal {
			    public void talk() {
					    System.out.println("I am a dog");
			    }
		    }
		    public class MyClass {
			    public static void main (String[] args) {
				    Animal dog = new Dog();
				    dog.talk();
			    }
		    }

			
	 - [ ] I am a dog
	 - [ ] I am an animal
	 - [ ] Empty string
	 - [ ] I am a pig
